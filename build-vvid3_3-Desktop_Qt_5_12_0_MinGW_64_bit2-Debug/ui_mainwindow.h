/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QFrame *line_5;
    QFrame *line_28;
    QComboBox *comboBox;
    QFrame *line_16;
    QFrame *line_6;
    QFrame *line_25;
    QLineEdit *LE_imaginary_part_2number;
    QFrame *line_21;
    QFrame *line_9;
    QFrame *line_26;
    QWidget *layoutWidget_3;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    QLineEdit *LE_real_part_1number;
    QComboBox *CB_action;
    QLabel *label_result;
    QFrame *line_22;
    QLineEdit *LE_imaginary_part_1number;
    QPushButton *pushButton;
    QFrame *line_8;
    QPushButton *PB_pow;
    QLabel *LB_imaginary_1;
    QLabel *LE_i_4;
    QFrame *line_20;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_4;
    QLineEdit *LE_real_result;
    QLabel *LE_i_2;
    QFrame *line_18;
    QFrame *line_3;
    QLabel *LE_i;
    QFrame *line_19;
    QFrame *line_11;
    QFrame *line_4;
    QLabel *LB_imaginary_2;
    QFrame *line_13;
    QFrame *line_12;
    QLabel *LE_i_6;
    QFrame *line_23;
    QLabel *label_8;
    QFrame *line_2;
    QWidget *layoutWidget_2;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_6;
    QLabel *LB_imaginary_3;
    QLineEdit *LE_real_part_for_tranform;
    QHBoxLayout *horizontalLayout_7;
    QLabel *LB_imaginary_4;
    QLineEdit *LE_imaginary_part_for_tranform;
    QPushButton *PB_transform;
    QHBoxLayout *horizontalLayout_10;
    QPushButton *PB_information;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_7;
    QLineEdit *LE_argument_for_tranform;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_10;
    QLineEdit *LE_module_for_tranform;
    QLabel *LE_i_5;
    QFrame *line_10;
    QFrame *line_7;
    QFrame *line_27;
    QLineEdit *LE_imaginary_result;
    QWidget *layoutWidget_4;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLineEdit *LE_real_part_2number;
    QFrame *line_24;
    QWidget *layoutWidget_5;
    QVBoxLayout *verticalLayout_6;
    QLineEdit *LE_result_argument;
    QLineEdit *LE_result_module;
    QFrame *line_14;
    QLabel *label_3;
    QLabel *LE_i_3;
    QPushButton *PB_information_2;
    QFrame *line;
    QFrame *line_15;
    QWidget *layoutWidget_6;
    QVBoxLayout *verticalLayout_5;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_6;
    QLineEdit *LE_argument;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_5;
    QLineEdit *LE_module;
    QFrame *line_17;
    QHBoxLayout *horizontalLayout;
    QLabel *Label_degree;
    QLineEdit *LE_degree;
    QTextEdit *textEdit;
    QGroupBox *groupBox;
    QLabel *LE_i_7;
    QWidget *layoutWidget_7;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_12;
    QLabel *label_9;
    QLineEdit *LE_argument_2;
    QHBoxLayout *horizontalLayout_13;
    QLabel *label_11;
    QLineEdit *LE_module_2;
    QLabel *LE_i_8;
    QLabel *label_title;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(940, 656);
        MainWindow->setMinimumSize(QSize(940, 656));
        MainWindow->setMaximumSize(QSize(940, 656));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        line_5 = new QFrame(centralWidget);
        line_5->setObjectName(QString::fromUtf8("line_5"));
        line_5->setGeometry(QRect(65, 118, 381, 20));
        line_5->setFrameShape(QFrame::HLine);
        line_5->setFrameShadow(QFrame::Sunken);
        line_28 = new QFrame(centralWidget);
        line_28->setObjectName(QString::fromUtf8("line_28"));
        line_28->setGeometry(QRect(-10, -10, 951, 44));
        line_28->setFrameShape(QFrame::HLine);
        line_28->setFrameShadow(QFrame::Sunken);
        comboBox = new QComboBox(centralWidget);
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->addItem(QString());
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        comboBox->setGeometry(QRect(73, 443, 221, 22));
        line_16 = new QFrame(centralWidget);
        line_16->setObjectName(QString::fromUtf8("line_16"));
        line_16->setGeometry(QRect(340, 610, 301, 20));
        line_16->setFrameShape(QFrame::HLine);
        line_16->setFrameShadow(QFrame::Sunken);
        line_6 = new QFrame(centralWidget);
        line_6->setObjectName(QString::fromUtf8("line_6"));
        line_6->setGeometry(QRect(65, 88, 381, 16));
        line_6->setFrameShape(QFrame::HLine);
        line_6->setFrameShadow(QFrame::Sunken);
        line_25 = new QFrame(centralWidget);
        line_25->setObjectName(QString::fromUtf8("line_25"));
        line_25->setGeometry(QRect(0, 535, 951, 51));
        line_25->setFrameShape(QFrame::HLine);
        line_25->setFrameShadow(QFrame::Sunken);
        LE_imaginary_part_2number = new QLineEdit(centralWidget);
        LE_imaginary_part_2number->setObjectName(QString::fromUtf8("LE_imaginary_part_2number"));
        LE_imaginary_part_2number->setGeometry(QRect(319, 100, 108, 22));
        LE_imaginary_part_2number->setMinimumSize(QSize(108, 0));
        LE_imaginary_part_2number->setMaximumSize(QSize(108, 16777215));
        line_21 = new QFrame(centralWidget);
        line_21->setObjectName(QString::fromUtf8("line_21"));
        line_21->setGeometry(QRect(64, 457, 386, 31));
        line_21->setFrameShape(QFrame::HLine);
        line_21->setFrameShadow(QFrame::Sunken);
        line_9 = new QFrame(centralWidget);
        line_9->setObjectName(QString::fromUtf8("line_9"));
        line_9->setGeometry(QRect(49, 307, 31, 165));
        line_9->setFrameShape(QFrame::VLine);
        line_9->setFrameShadow(QFrame::Sunken);
        line_26 = new QFrame(centralWidget);
        line_26->setObjectName(QString::fromUtf8("line_26"));
        line_26->setGeometry(QRect(0, 10, 21, 550));
        line_26->setFrameShape(QFrame::VLine);
        line_26->setFrameShadow(QFrame::Sunken);
        layoutWidget_3 = new QWidget(centralWidget);
        layoutWidget_3->setObjectName(QString::fromUtf8("layoutWidget_3"));
        layoutWidget_3->setGeometry(QRect(75, 68, 221, 24));
        horizontalLayout_3 = new QHBoxLayout(layoutWidget_3);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget_3);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout_3->addWidget(label);

        LE_real_part_1number = new QLineEdit(layoutWidget_3);
        LE_real_part_1number->setObjectName(QString::fromUtf8("LE_real_part_1number"));
        LE_real_part_1number->setMinimumSize(QSize(108, 0));
        LE_real_part_1number->setMaximumSize(QSize(108, 16777215));

        horizontalLayout_3->addWidget(LE_real_part_1number);

        CB_action = new QComboBox(centralWidget);
        CB_action->addItem(QString());
        CB_action->addItem(QString());
        CB_action->addItem(QString());
        CB_action->addItem(QString());
        CB_action->addItem(QString());
        CB_action->setObjectName(QString::fromUtf8("CB_action"));
        CB_action->setGeometry(QRect(70, 182, 227, 25));
        CB_action->setMinimumSize(QSize(227, 0));
        CB_action->setMaximumSize(QSize(227, 16777215));
        label_result = new QLabel(centralWidget);
        label_result->setObjectName(QString::fromUtf8("label_result"));
        label_result->setGeometry(QRect(540, 261, 121, 21));
        label_result->setMinimumSize(QSize(98, 0));
        line_22 = new QFrame(centralWidget);
        line_22->setObjectName(QString::fromUtf8("line_22"));
        line_22->setGeometry(QRect(50, 35, 31, 181));
        line_22->setFrameShape(QFrame::VLine);
        line_22->setFrameShadow(QFrame::Sunken);
        LE_imaginary_part_1number = new QLineEdit(centralWidget);
        LE_imaginary_part_1number->setObjectName(QString::fromUtf8("LE_imaginary_part_1number"));
        LE_imaginary_part_1number->setGeometry(QRect(320, 70, 108, 22));
        LE_imaginary_part_1number->setMinimumSize(QSize(108, 0));
        LE_imaginary_part_1number->setMaximumSize(QSize(108, 16777215));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(305, 182, 135, 25));
        pushButton->setMinimumSize(QSize(123, 0));
        pushButton->setMaximumSize(QSize(1222, 16777215));
        pushButton->setSizeIncrement(QSize(125, 0));
        line_8 = new QFrame(centralWidget);
        line_8->setObjectName(QString::fromUtf8("line_8"));
        line_8->setGeometry(QRect(435, 35, 20, 181));
        line_8->setFrameShape(QFrame::VLine);
        line_8->setFrameShadow(QFrame::Sunken);
        PB_pow = new QPushButton(centralWidget);
        PB_pow->setObjectName(QString::fromUtf8("PB_pow"));
        PB_pow->setGeometry(QRect(310, 442, 135, 25));
        PB_pow->setMinimumSize(QSize(135, 25));
        PB_pow->setMaximumSize(QSize(135, 25));
        LB_imaginary_1 = new QLabel(centralWidget);
        LB_imaginary_1->setObjectName(QString::fromUtf8("LB_imaginary_1"));
        LB_imaginary_1->setGeometry(QRect(312, 42, 121, 20));
        LE_i_4 = new QLabel(centralWidget);
        LE_i_4->setObjectName(QString::fromUtf8("LE_i_4"));
        LE_i_4->setGeometry(QRect(430, 329, 16, 22));
        line_20 = new QFrame(centralWidget);
        line_20->setObjectName(QString::fromUtf8("line_20"));
        line_20->setGeometry(QRect(63, 297, 387, 20));
        line_20->setFrameShape(QFrame::HLine);
        line_20->setFrameShadow(QFrame::Sunken);
        layoutWidget = new QWidget(centralWidget);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(75, 138, 221, 33));
        horizontalLayout_11 = new QHBoxLayout(layoutWidget);
        horizontalLayout_11->setSpacing(6);
        horizontalLayout_11->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        horizontalLayout_11->setContentsMargins(0, 0, 0, 0);
        label_4 = new QLabel(layoutWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setMinimumSize(QSize(98, 31));
        label_4->setMaximumSize(QSize(16777215, 31));

        horizontalLayout_11->addWidget(label_4);

        LE_real_result = new QLineEdit(layoutWidget);
        LE_real_result->setObjectName(QString::fromUtf8("LE_real_result"));
        LE_real_result->setEnabled(false);
        LE_real_result->setMinimumSize(QSize(108, 31));
        LE_real_result->setMaximumSize(QSize(108, 31));
        LE_real_result->setStyleSheet(QString::fromUtf8("background:rgb(228, 255, 196)"));
        LE_real_result->setDragEnabled(false);

        horizontalLayout_11->addWidget(LE_real_result);

        LE_i_2 = new QLabel(centralWidget);
        LE_i_2->setObjectName(QString::fromUtf8("LE_i_2"));
        LE_i_2->setGeometry(QRect(433, 100, 16, 19));
        line_18 = new QFrame(centralWidget);
        line_18->setObjectName(QString::fromUtf8("line_18"));
        line_18->setGeometry(QRect(68, 425, 381, 21));
        line_18->setFrameShape(QFrame::HLine);
        line_18->setFrameShadow(QFrame::Sunken);
        line_3 = new QFrame(centralWidget);
        line_3->setObjectName(QString::fromUtf8("line_3"));
        line_3->setGeometry(QRect(65, 168, 381, 16));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);
        LE_i = new QLabel(centralWidget);
        LE_i->setObjectName(QString::fromUtf8("LE_i"));
        LE_i->setGeometry(QRect(434, 70, 16, 19));
        line_19 = new QFrame(centralWidget);
        line_19->setObjectName(QString::fromUtf8("line_19"));
        line_19->setGeometry(QRect(267, 373, 181, 31));
        line_19->setFrameShape(QFrame::HLine);
        line_19->setFrameShadow(QFrame::Sunken);
        line_11 = new QFrame(centralWidget);
        line_11->setObjectName(QString::fromUtf8("line_11"));
        line_11->setGeometry(QRect(430, 307, 41, 165));
        line_11->setFrameShape(QFrame::VLine);
        line_11->setFrameShadow(QFrame::Sunken);
        line_4 = new QFrame(centralWidget);
        line_4->setObjectName(QString::fromUtf8("line_4"));
        line_4->setGeometry(QRect(65, 208, 381, 16));
        line_4->setFrameShape(QFrame::HLine);
        line_4->setFrameShadow(QFrame::Sunken);
        LB_imaginary_2 = new QLabel(centralWidget);
        LB_imaginary_2->setObjectName(QString::fromUtf8("LB_imaginary_2"));
        LB_imaginary_2->setGeometry(QRect(105, 42, 171, 20));
        line_13 = new QFrame(centralWidget);
        line_13->setObjectName(QString::fromUtf8("line_13"));
        line_13->setGeometry(QRect(851, 44, 41, 161));
        line_13->setFrameShape(QFrame::VLine);
        line_13->setFrameShadow(QFrame::Sunken);
        line_12 = new QFrame(centralWidget);
        line_12->setObjectName(QString::fromUtf8("line_12"));
        line_12->setGeometry(QRect(560, 44, 20, 161));
        line_12->setFrameShape(QFrame::VLine);
        line_12->setFrameShadow(QFrame::Sunken);
        LE_i_6 = new QLabel(centralWidget);
        LE_i_6->setObjectName(QString::fromUtf8("LE_i_6"));
        LE_i_6->setGeometry(QRect(432, 142, 16, 19));
        line_23 = new QFrame(centralWidget);
        line_23->setObjectName(QString::fromUtf8("line_23"));
        line_23->setGeometry(QRect(473, 10, 31, 551));
        line_23->setFrameShape(QFrame::VLine);
        line_23->setFrameShadow(QFrame::Sunken);
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(313, 307, 121, 18));
        label_8->setMinimumSize(QSize(98, 0));
        line_2 = new QFrame(centralWidget);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setGeometry(QRect(288, 35, 31, 181));
        line_2->setFrameShape(QFrame::VLine);
        line_2->setFrameShadow(QFrame::Sunken);
        layoutWidget_2 = new QWidget(centralWidget);
        layoutWidget_2->setObjectName(QString::fromUtf8("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(581, 49, 285, 150));
        verticalLayout_3 = new QVBoxLayout(layoutWidget_2);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        LB_imaginary_3 = new QLabel(layoutWidget_2);
        LB_imaginary_3->setObjectName(QString::fromUtf8("LB_imaginary_3"));
        LB_imaginary_3->setMinimumSize(QSize(151, 22));
        LB_imaginary_3->setMaximumSize(QSize(151, 22));

        horizontalLayout_6->addWidget(LB_imaginary_3);

        LE_real_part_for_tranform = new QLineEdit(layoutWidget_2);
        LE_real_part_for_tranform->setObjectName(QString::fromUtf8("LE_real_part_for_tranform"));
        LE_real_part_for_tranform->setMinimumSize(QSize(108, 0));
        LE_real_part_for_tranform->setMaximumSize(QSize(108, 16777215));

        horizontalLayout_6->addWidget(LE_real_part_for_tranform);


        verticalLayout->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        LB_imaginary_4 = new QLabel(layoutWidget_2);
        LB_imaginary_4->setObjectName(QString::fromUtf8("LB_imaginary_4"));
        LB_imaginary_4->setMinimumSize(QSize(151, 22));
        LB_imaginary_4->setMaximumSize(QSize(151, 22));

        horizontalLayout_7->addWidget(LB_imaginary_4);

        LE_imaginary_part_for_tranform = new QLineEdit(layoutWidget_2);
        LE_imaginary_part_for_tranform->setObjectName(QString::fromUtf8("LE_imaginary_part_for_tranform"));
        LE_imaginary_part_for_tranform->setMinimumSize(QSize(108, 0));
        LE_imaginary_part_for_tranform->setMaximumSize(QSize(108, 16777215));

        horizontalLayout_7->addWidget(LE_imaginary_part_for_tranform);


        verticalLayout->addLayout(horizontalLayout_7);


        verticalLayout_3->addLayout(verticalLayout);

        PB_transform = new QPushButton(layoutWidget_2);
        PB_transform->setObjectName(QString::fromUtf8("PB_transform"));
        PB_transform->setMinimumSize(QSize(0, 22));
        PB_transform->setMaximumSize(QSize(66666, 22));

        verticalLayout_3->addWidget(PB_transform);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        PB_information = new QPushButton(layoutWidget_2);
        PB_information->setObjectName(QString::fromUtf8("PB_information"));
        PB_information->setMinimumSize(QSize(70, 50));
        PB_information->setMaximumSize(QSize(70, 50));

        horizontalLayout_10->addWidget(PB_information);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(6);
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_7 = new QLabel(layoutWidget_2);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setMinimumSize(QSize(81, 22));
        label_7->setMaximumSize(QSize(81, 22));

        horizontalLayout_9->addWidget(label_7);

        LE_argument_for_tranform = new QLineEdit(layoutWidget_2);
        LE_argument_for_tranform->setObjectName(QString::fromUtf8("LE_argument_for_tranform"));
        LE_argument_for_tranform->setMinimumSize(QSize(108, 22));
        LE_argument_for_tranform->setMaximumSize(QSize(108, 22));

        horizontalLayout_9->addWidget(LE_argument_for_tranform);


        verticalLayout_2->addLayout(horizontalLayout_9);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setSpacing(6);
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_10 = new QLabel(layoutWidget_2);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setMinimumSize(QSize(81, 22));
        label_10->setMaximumSize(QSize(81, 22));

        horizontalLayout_8->addWidget(label_10);

        LE_module_for_tranform = new QLineEdit(layoutWidget_2);
        LE_module_for_tranform->setObjectName(QString::fromUtf8("LE_module_for_tranform"));
        LE_module_for_tranform->setMinimumSize(QSize(108, 22));
        LE_module_for_tranform->setMaximumSize(QSize(108, 22));

        horizontalLayout_8->addWidget(LE_module_for_tranform);


        verticalLayout_2->addLayout(horizontalLayout_8);


        horizontalLayout_10->addLayout(verticalLayout_2);


        verticalLayout_3->addLayout(horizontalLayout_10);

        LE_i_5 = new QLabel(centralWidget);
        LE_i_5->setObjectName(QString::fromUtf8("LE_i_5"));
        LE_i_5->setGeometry(QRect(859, 140, 21, 25));
        line_10 = new QFrame(centralWidget);
        line_10->setObjectName(QString::fromUtf8("line_10"));
        line_10->setGeometry(QRect(65, 28, 381, 16));
        line_10->setFrameShape(QFrame::HLine);
        line_10->setFrameShadow(QFrame::Sunken);
        line_7 = new QFrame(centralWidget);
        line_7->setObjectName(QString::fromUtf8("line_7"));
        line_7->setGeometry(QRect(65, 58, 381, 16));
        line_7->setFrameShape(QFrame::HLine);
        line_7->setFrameShadow(QFrame::Sunken);
        line_27 = new QFrame(centralWidget);
        line_27->setObjectName(QString::fromUtf8("line_27"));
        line_27->setGeometry(QRect(920, 10, 21, 550));
        line_27->setFrameShape(QFrame::VLine);
        line_27->setFrameShadow(QFrame::Sunken);
        LE_imaginary_result = new QLineEdit(centralWidget);
        LE_imaginary_result->setObjectName(QString::fromUtf8("LE_imaginary_result"));
        LE_imaginary_result->setEnabled(false);
        LE_imaginary_result->setGeometry(QRect(319, 138, 108, 31));
        LE_imaginary_result->setMinimumSize(QSize(108, 0));
        LE_imaginary_result->setMaximumSize(QSize(108, 16777215));
        LE_imaginary_result->setStyleSheet(QString::fromUtf8("background:rgb(228, 255, 196)"));
        LE_imaginary_result->setDragEnabled(false);
        layoutWidget_4 = new QWidget(centralWidget);
        layoutWidget_4->setObjectName(QString::fromUtf8("layoutWidget_4"));
        layoutWidget_4->setGeometry(QRect(75, 98, 221, 24));
        horizontalLayout_2 = new QHBoxLayout(layoutWidget_4);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(layoutWidget_4);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        LE_real_part_2number = new QLineEdit(layoutWidget_4);
        LE_real_part_2number->setObjectName(QString::fromUtf8("LE_real_part_2number"));
        LE_real_part_2number->setMinimumSize(QSize(108, 0));
        LE_real_part_2number->setMaximumSize(QSize(108, 16777215));

        horizontalLayout_2->addWidget(LE_real_part_2number);

        line_24 = new QFrame(centralWidget);
        line_24->setObjectName(QString::fromUtf8("line_24"));
        line_24->setGeometry(QRect(0, 245, 941, 31));
        line_24->setFrameShape(QFrame::HLine);
        line_24->setFrameShadow(QFrame::Sunken);
        layoutWidget_5 = new QWidget(centralWidget);
        layoutWidget_5->setObjectName(QString::fromUtf8("layoutWidget_5"));
        layoutWidget_5->setGeometry(QRect(320, 330, 110, 52));
        verticalLayout_6 = new QVBoxLayout(layoutWidget_5);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        LE_result_argument = new QLineEdit(layoutWidget_5);
        LE_result_argument->setObjectName(QString::fromUtf8("LE_result_argument"));
        LE_result_argument->setEnabled(false);
        LE_result_argument->setMinimumSize(QSize(108, 0));
        LE_result_argument->setMaximumSize(QSize(108, 16777215));
        LE_result_argument->setStyleSheet(QString::fromUtf8("background:rgb(228, 255, 196)"));
        LE_result_argument->setDragEnabled(false);

        verticalLayout_6->addWidget(LE_result_argument);

        LE_result_module = new QLineEdit(layoutWidget_5);
        LE_result_module->setObjectName(QString::fromUtf8("LE_result_module"));
        LE_result_module->setEnabled(false);
        LE_result_module->setMinimumSize(QSize(108, 0));
        LE_result_module->setMaximumSize(QSize(108, 16777215));
        LE_result_module->setStyleSheet(QString::fromUtf8("background:rgb(228, 255, 196)"));
        LE_result_module->setDragEnabled(false);

        verticalLayout_6->addWidget(LE_result_module);

        line_14 = new QFrame(centralWidget);
        line_14->setObjectName(QString::fromUtf8("line_14"));
        line_14->setGeometry(QRect(570, 190, 301, 31));
        line_14->setFrameShape(QFrame::HLine);
        line_14->setFrameShadow(QFrame::Sunken);
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(340, 630, 301, 21));
        QFont font;
        font.setFamily(QString::fromUtf8("Segoe Script"));
        font.setPointSize(11);
        font.setBold(true);
        font.setWeight(75);
        label_3->setFont(font);
        LE_i_3 = new QLabel(centralWidget);
        LE_i_3->setObjectName(QString::fromUtf8("LE_i_3"));
        LE_i_3->setGeometry(QRect(287, 327, 16, 22));
        PB_information_2 = new QPushButton(centralWidget);
        PB_information_2->setObjectName(QString::fromUtf8("PB_information_2"));
        PB_information_2->setGeometry(QRect(310, 400, 135, 22));
        PB_information_2->setMinimumSize(QSize(135, 22));
        PB_information_2->setMaximumSize(QSize(135, 22));
        line = new QFrame(centralWidget);
        line->setObjectName(QString::fromUtf8("line"));
        line->setGeometry(QRect(290, 307, 21, 165));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        line_15 = new QFrame(centralWidget);
        line_15->setObjectName(QString::fromUtf8("line_15"));
        line_15->setGeometry(QRect(570, 35, 299, 21));
        line_15->setFrameShape(QFrame::HLine);
        line_15->setFrameShadow(QFrame::Sunken);
        layoutWidget_6 = new QWidget(centralWidget);
        layoutWidget_6->setObjectName(QString::fromUtf8("layoutWidget_6"));
        layoutWidget_6->setGeometry(QRect(68, 327, 218, 97));
        verticalLayout_5 = new QVBoxLayout(layoutWidget_6);
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_6 = new QLabel(layoutWidget_6);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setMinimumSize(QSize(98, 0));

        horizontalLayout_5->addWidget(label_6);

        LE_argument = new QLineEdit(layoutWidget_6);
        LE_argument->setObjectName(QString::fromUtf8("LE_argument"));
        LE_argument->setMinimumSize(QSize(108, 0));
        LE_argument->setMaximumSize(QSize(108, 16777215));

        horizontalLayout_5->addWidget(LE_argument);


        verticalLayout_4->addLayout(horizontalLayout_5);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        label_5 = new QLabel(layoutWidget_6);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setMinimumSize(QSize(98, 0));

        horizontalLayout_4->addWidget(label_5);

        LE_module = new QLineEdit(layoutWidget_6);
        LE_module->setObjectName(QString::fromUtf8("LE_module"));
        LE_module->setMinimumSize(QSize(108, 0));
        LE_module->setMaximumSize(QSize(108, 16777215));

        horizontalLayout_4->addWidget(LE_module);


        verticalLayout_4->addLayout(horizontalLayout_4);


        verticalLayout_5->addLayout(verticalLayout_4);

        line_17 = new QFrame(layoutWidget_6);
        line_17->setObjectName(QString::fromUtf8("line_17"));
        line_17->setFrameShape(QFrame::HLine);
        line_17->setFrameShadow(QFrame::Sunken);

        verticalLayout_5->addWidget(line_17);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        Label_degree = new QLabel(layoutWidget_6);
        Label_degree->setObjectName(QString::fromUtf8("Label_degree"));
        Label_degree->setMinimumSize(QSize(98, 0));
        Label_degree->setTextFormat(Qt::RichText);

        horizontalLayout->addWidget(Label_degree);

        LE_degree = new QLineEdit(layoutWidget_6);
        LE_degree->setObjectName(QString::fromUtf8("LE_degree"));
        LE_degree->setMinimumSize(QSize(108, 0));
        LE_degree->setMaximumSize(QSize(108, 16777215));

        horizontalLayout->addWidget(LE_degree);


        verticalLayout_5->addLayout(horizontalLayout);

        textEdit = new QTextEdit(centralWidget);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        textEdit->setEnabled(true);
        textEdit->setGeometry(QRect(540, 280, 361, 271));
        QFont font1;
        font1.setPointSize(11);
        textEdit->setFont(font1);
        textEdit->setStyleSheet(QString::fromUtf8("background:rgb(228, 255, 196)"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(70, 480, 271, 80));
        LE_i_7 = new QLabel(groupBox);
        LE_i_7->setObjectName(QString::fromUtf8("LE_i_7"));
        LE_i_7->setGeometry(QRect(287, 347, 16, 22));
        layoutWidget_7 = new QWidget(groupBox);
        layoutWidget_7->setObjectName(QString::fromUtf8("layoutWidget_7"));
        layoutWidget_7->setGeometry(QRect(0, 20, 216, 56));
        verticalLayout_7 = new QVBoxLayout(layoutWidget_7);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        verticalLayout_7->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setSpacing(6);
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        label_9 = new QLabel(layoutWidget_7);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setMinimumSize(QSize(98, 0));

        horizontalLayout_12->addWidget(label_9);

        LE_argument_2 = new QLineEdit(layoutWidget_7);
        LE_argument_2->setObjectName(QString::fromUtf8("LE_argument_2"));
        LE_argument_2->setMinimumSize(QSize(108, 0));
        LE_argument_2->setMaximumSize(QSize(108, 16777215));

        horizontalLayout_12->addWidget(LE_argument_2);


        verticalLayout_7->addLayout(horizontalLayout_12);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setSpacing(6);
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        label_11 = new QLabel(layoutWidget_7);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setMinimumSize(QSize(98, 0));

        horizontalLayout_13->addWidget(label_11);

        LE_module_2 = new QLineEdit(layoutWidget_7);
        LE_module_2->setObjectName(QString::fromUtf8("LE_module_2"));
        LE_module_2->setMinimumSize(QSize(108, 0));
        LE_module_2->setMaximumSize(QSize(108, 16777215));

        horizontalLayout_13->addWidget(LE_module_2);


        verticalLayout_7->addLayout(horizontalLayout_13);

        LE_i_8 = new QLabel(groupBox);
        LE_i_8->setObjectName(QString::fromUtf8("LE_i_8"));
        LE_i_8->setGeometry(QRect(220, 20, 16, 22));
        label_title = new QLabel(groupBox);
        label_title->setObjectName(QString::fromUtf8("label_title"));
        label_title->setGeometry(QRect(0, 0, 91, 20));
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        comboBox->setItemText(0, QApplication::translate("MainWindow", "\320\222\320\276\320\267\320\262\320\265\321\201\321\202\320\270 \320\262 \321\201\321\202\320\265\320\277\320\265\320\275\321\214", nullptr));
        comboBox->setItemText(1, QApplication::translate("MainWindow", "\320\222\321\213\321\207\320\270\321\201\320\273\320\270\321\202\321\214 \320\272\320\276\321\200\320\265\320\275\321\214", nullptr));
        comboBox->setItemText(2, QApplication::translate("MainWindow", "\320\243\320\274\320\275\320\276\320\266\320\265\320\275\320\270\320\265", nullptr));
        comboBox->setItemText(3, QApplication::translate("MainWindow", "\320\224\320\265\320\273\320\265\320\275\320\270\320\265", nullptr));

        label->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:11pt; color:#e6e672;\">\320\237\320\265\321\200\320\262\320\276\320\265 \321\207\320\270\321\201\320\273\320\276:</span></p></body></html>", nullptr));
        CB_action->setItemText(0, QApplication::translate("MainWindow", "\320\222\321\213\320\261\320\265\321\200\320\270\321\202\320\265 \320\264\320\265\320\271\321\201\321\202\320\262\320\270\320\265:", nullptr));
        CB_action->setItemText(1, QApplication::translate("MainWindow", "\320\241\321\203\320\274\320\274\320\260", nullptr));
        CB_action->setItemText(2, QApplication::translate("MainWindow", "\320\222\321\213\321\207\320\270\321\202\320\260\320\275\320\270\320\265", nullptr));
        CB_action->setItemText(3, QApplication::translate("MainWindow", "\320\237\321\200\320\276\320\270\320\267\320\262\320\265\320\264\320\265\320\275\320\270\320\265", nullptr));
        CB_action->setItemText(4, QApplication::translate("MainWindow", "\320\237\321\200\320\276\320\262\320\265\321\200\320\272\320\260 \320\275\320\260 \321\200\320\260\320\262\320\265\320\275\321\201\321\202\320\262\320\276", nullptr));

        label_result->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:11pt; color:#e6e672;\">\320\236\321\202\320\262\320\265\321\202:</span></p></body></html>", nullptr));
        pushButton->setText(QApplication::translate("MainWindow", "\320\222\321\213\321\207\320\270\321\201\320\273\320\270\321\202\321\214", nullptr));
        PB_pow->setText(QApplication::translate("MainWindow", "\320\222\320\276\320\267\320\262\320\265\321\201\321\202\320\270 \320\262 \321\201\321\202\320\265\320\277\320\265\320\275\321\214.", nullptr));
        LB_imaginary_1->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:10pt; color:#e6e672;\">\320\234\320\275\320\270\320\274\320\260\321\217 \321\207\320\260\321\201\321\202\321\214 :</span></p></body></html>", nullptr));
        LE_i_4->setText(QApplication::translate("MainWindow", "<html><head/><body><p><a name=\"MathJax-Span-17\"/><span style=\" font-family:'MathJax_Math'; font-size:11pt; font-style:italic; color:#e6e672; background-color:transparent;\">\317\200</span></p></body></html>", nullptr));
        label_4->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"right\"><span style=\" font-size:11pt; color:#e6e672;\">\320\236\321\202\320\262\320\265\321\202:</span></p></body></html>", nullptr));
        LE_i_2->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt; color:#e6e672;\">i</span></p></body></html>", nullptr));
        LE_i->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt; color:#e6e672;\">i</span></p></body></html>", nullptr));
        LB_imaginary_2->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:10pt; color:#e6e672;\">\320\224\320\265\320\271\321\201\321\202\320\262\320\270\321\202\320\265\320\273\321\214\320\275\320\260\321\217 \321\207\320\260\321\201\321\202\321\214 :</span></p></body></html>", nullptr));
        LE_i_6->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:12pt; color:#e6e672;\">i</span></p></body></html>", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:11pt; color:#e6e672;\">\320\236\321\202\320\262\320\265\321\202:</span></p></body></html>", nullptr));
        LB_imaginary_3->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"right\"><span style=\" font-size:10pt; color:#e6e672;\">\320\224\320\265\320\271\321\201\321\202\320\262\320\270\321\202\320\265\320\273\321\214\320\275\320\260\321\217 \321\207\320\260\321\201\321\202\321\214 :</span></p></body></html>", nullptr));
        LB_imaginary_4->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"right\"><span style=\" font-size:10pt; color:#e6e672;\">\320\234\320\275\320\270\320\274\320\260\321\217 \321\207\320\260\321\201\321\202\321\214 :</span></p></body></html>", nullptr));
        PB_transform->setText(QApplication::translate("MainWindow", "\320\277\320\265\321\200\320\265\320\262\320\265\321\201\321\202\320\270", nullptr));
        PB_information->setText(QApplication::translate("MainWindow", "\320\241\320\277\321\200\320\260\320\262\320\272\320\260", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"right\"><span style=\" font-size:10pt; color:#e6e672;\">\320\220\321\200\320\263\321\203\320\274\320\265\320\275\321\202 </span><span style=\" font-family:'Arial,Helvetica,sans-serif'; font-size:10pt; color:#e6e672; background-color:transparent;\">\317\206 :</span></p></body></html>", nullptr));
        label_10->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"right\"><span style=\" font-size:10pt; color:#e6e672;\">\320\234\320\276\320\264\321\203\320\273\321\214 r :</span></p></body></html>", nullptr));
        LE_i_5->setText(QApplication::translate("MainWindow", "<html><head/><body><p><a name=\"MathJax-Span-17\"/><span style=\" font-family:'MathJax_Math'; font-size:11pt; font-style:italic; color:#e6e672; background-color:transparent;\">\317\200</span></p></body></html>", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:11pt; color:#e6e672;\">\320\222\321\202\320\276\321\200\320\276\320\265 \321\207\320\270\321\201\320\273\320\276:</span></p></body></html>", nullptr));
        label_3->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-style:italic; color:#e6e672;\">\320\234\320\265\320\273\321\214\320\275\320\270\320\272\320\276\320\262 \320\230.\320\224.</span></p></body></html>", nullptr));
        LE_i_3->setText(QApplication::translate("MainWindow", "<html><head/><body><p><a name=\"MathJax-Span-17\"/><span style=\" font-family:'MathJax_Math'; font-size:11pt; font-style:italic; color:#e6e672; background-color:transparent;\">\317\200</span></p></body></html>", nullptr));
        PB_information_2->setText(QApplication::translate("MainWindow", "\320\241\320\277\321\200\320\260\320\262\320\272\320\260", nullptr));
        label_6->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"right\"><span style=\" font-size:11pt; color:#e6e672;\">\320\220\321\200\320\263\321\203\320\274\320\265\320\275\321\202 </span><span style=\" font-family:'Arial,Helvetica,sans-serif'; font-size:11pt; color:#e6e672; background-color:transparent;\">\317\206:</span></p></body></html>", nullptr));
        label_5->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"right\"><span style=\" font-size:11pt; color:#e6e672;\">\320\234\320\276\320\264\321\203\320\273\321\214 r:</span></p></body></html>", nullptr));
        Label_degree->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"right\"><span style=\" font-size:11pt; color:#e6e672;\">\320\237\320\276\320\272\320\260\320\267\320\260\321\202\320\265\320\273\321\214:</span></p></body></html>", nullptr));
        groupBox->setTitle(QString());
        LE_i_7->setText(QApplication::translate("MainWindow", "<html><head/><body><p><a name=\"MathJax-Span-17\"/><span style=\" font-family:'MathJax_Math'; font-size:11pt; font-style:italic; color:#e6e672; background-color:transparent;\">\317\200</span></p></body></html>", nullptr));
        label_9->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"right\"><span style=\" font-size:11pt; color:#e6e672;\">\320\220\321\200\320\263\321\203\320\274\320\265\320\275\321\202 </span><span style=\" font-family:'Arial,Helvetica,sans-serif'; font-size:11pt; color:#e6e672; background-color:transparent;\">\317\206:</span></p></body></html>", nullptr));
        label_11->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"right\"><span style=\" font-size:11pt; color:#e6e672;\">\320\234\320\276\320\264\321\203\320\273\321\214 r:</span></p></body></html>", nullptr));
        LE_i_8->setText(QApplication::translate("MainWindow", "<html><head/><body><p><a name=\"MathJax-Span-17\"/><span style=\" font-family:'MathJax_Math'; font-size:11pt; font-style:italic; color:#e6e672; background-color:transparent;\">\317\200</span></p></body></html>", nullptr));
        label_title->setText(QApplication::translate("MainWindow", "<html><head/><body><p><span style=\" color:#dddd6e;\">\320\222\321\202\320\276\321\200\320\276\320\265 \321\207\320\270\321\201\320\273\320\276 :</span></p></body></html>", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
