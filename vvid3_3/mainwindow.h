#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_PB_pow_clicked();

    void on_pushButton_clicked();

    void on_PB_information_2_clicked();

    void on_PB_information_clicked();

    void on_comboBox_currentIndexChanged(int index);

    void on_PB_transform_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
