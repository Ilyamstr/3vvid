#include "functions_pack.h"
#include <math.h>
#include <QMessageBox>

Complex_number sum_number(Complex_number first_number, Complex_number second_number)
{
    Complex_number result_sum;
    result_sum.real_part = first_number.real_part + second_number.real_part;
    result_sum.imaginary_part = first_number.imaginary_part + second_number.imaginary_part;

    return  result_sum;
}

Complex_number difference_number(Complex_number first_number, Complex_number second_number)
{
    Complex_number result_difference;
    result_difference.real_part = first_number.real_part - second_number.real_part;
    result_difference.imaginary_part = first_number.imaginary_part - second_number.imaginary_part;

    return result_difference;
}

bool equality_test(Complex_number first_number, Complex_number second_number)
{
    if (first_number.real_part == second_number.real_part && first_number.imaginary_part == second_number.imaginary_part)
    {
        return true;
    }
    else {
         return false;
    }
}

Complex_number_another_form exponentiation_number(Complex_number_another_form number, double degree)
{
    Complex_number_another_form result_number;
    result_number.module = pow(number.module, degree);
    result_number.argument = number.argument * degree;

    return result_number;
}

Complex_number product_number(Complex_number first_number, Complex_number second_number)
{
    Complex_number result_number;
    result_number.real_part = (first_number.real_part * second_number.real_part) - (first_number.imaginary_part *
                                                                                    second_number.imaginary_part);
    result_number.imaginary_part = (first_number.real_part * second_number.imaginary_part) + (second_number.real_part *
                                                                                              first_number.imaginary_part);

    return result_number;
}

QVector<Complex_number_another_form> radical(Complex_number_another_form number, double degree)
{
    QVector<Complex_number_another_form> temp;
    temp.resize(degree);
    double radical = 1 / degree;
    QString str = QString::number(radical);
    QMessageBox::information(nullptr, "", str);
    for (int k = 0; k < degree; k++)
    {
        temp[k].module = pow(number.module, radical);
        temp[k].argument = (number.argument + 8 * atan(1) * k) / 2;
    }
    return temp;
}


void transform_number_from_another_form(double module, double argument, double& real_part, double& imaginary_part)
{
    while (argument >= 2)
    {
        argument = argument - 2;
    }
    argument = argument * 4 * atan(1);
    real_part = module * cos(argument);
    imaginary_part = module * sin(argument);

    if (argument == 4 * atan(1) || argument == 0)
    {
        imaginary_part = 0;
    }
    if (argument == 2 * atan(1) || argument == 6 * atan(1))
    {
        real_part = 0;
    }
}

void transform_number(double& module, double& argument, double real_part, double imaginary_part)
{
    module = sqrt(pow(real_part, 2) + pow(imaginary_part, 2));
    argument = atan(imaginary_part / real_part) / (4 * atan(1));

    if (real_part < 0 && imaginary_part < 0)
    {
        argument++;
    }
    else if (real_part < 0)
    {
        if (argument < 0)
        {
            argument = argument * (-1);
        }
        argument = 1 - argument;
    }
    else if (imaginary_part < 0)
    {
        argument = argument + 2;
    }
}
