#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "functions_pack.h"
#include <QRegExp>
#include <QMessageBox>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //Устанавливаем фон
    QPixmap backgroundnd("C:/Users/iljam/OneDrive/Desktop/img.jpg");
    backgroundnd = backgroundnd.scaled(this->size(), Qt::IgnoreAspectRatio);  //Необходимо для фона
    QPalette palette;
    palette.setBrush(QPalette::Background, backgroundnd);
    this->setPalette(palette);

    //Регулярные выражения(+1 к защите от дурака)
    ui->textEdit->hide();
    ui->label_result->hide();
    QRegExp exp("^[-+]?[0-9]+[.,]?[0-9]+(?:[eE][-+]?[0-9]+)?$");
    ui->LE_real_part_1number->setValidator(new QRegExpValidator(exp, this));
    ui->LE_real_part_2number->setValidator(new QRegExpValidator(exp, this));
    ui->LE_imaginary_part_2number->setValidator(new QRegExpValidator(exp, this));
    ui->LE_imaginary_part_1number->setValidator(new QRegExpValidator(exp, this));
    ui->LE_module->setValidator(new QRegExpValidator(exp, this));
    QRegExp exp_for_argument("^[0-1][.,][0-9]+$");
    ui->LE_argument->setValidator(new QRegExpValidator(exp_for_argument, this));
    QRegExp exp_for_degree("^[1-9][0-9]{0,10}$");
    ui->LE_degree->setValidator(new QRegExpValidator(exp_for_degree, this));
}

MainWindow::~MainWindow()
{
    delete ui;
}




void MainWindow::on_PB_pow_clicked()            //кнопка вычисления корня или возведения в степень
{
    if (ui->LE_degree->text().isEmpty() || ui->LE_module->text().isEmpty() || ui->LE_argument->text().isEmpty())
    {
        QMessageBox::critical(this, "Ошибка", "Не все поля заполнены.");
    }
    else
    {

        Complex_number_another_form number;
        number.module = ui->LE_module->text().toDouble();
        number.argument = ui->LE_argument->text().toDouble();

        switch (ui->comboBox->currentIndex())       //зависит от выбора пользователя
        {
            case 0:
        {
                double degree = ui->LE_degree->text().toInt();
                number = exponentiation_number( number, degree);
                QString result = QString::number(number.module);
                ui->LE_result_module->setText(result);
                result = QString::number(number.argument);
                ui->LE_result_argument->setText(result);
        }
            break;

            case 1:
        {
                ui->textEdit->show();
                ui->label_result->show();
                double degree = ui->LE_degree->text().toInt();
                QVector<Complex_number_another_form> temp_vector = radical(number, degree);
                QString result;
                for (int k = 0; k < degree; k++)
                {
                    QString str = QString::number(temp_vector[k].module);
                    QMessageBox::information(this, "ok", str);
                    result.append(QString::number(k+1));
                    result.append(". z = ");
                    result.append(QString::number(temp_vector[k].module));
                    result.append(" * (cos(");
                    result.append(QString::number(temp_vector[k].argument));
                    result.append(") + i * sin(");
                    result.append(QString::number(temp_vector[k].argument));
                    result.append(") )\n");
                }
                ui->textEdit->setText(result);
        }
            break;

        }

    }
}


void MainWindow::on_pushButton_clicked()        //кнопка выполнения действий с комплексными числами в алгебраической форме
{
    if (ui->LE_real_part_1number->text().isEmpty() || ui->LE_real_part_2number->text().isEmpty() ||
            ui->LE_imaginary_part_1number->text().isEmpty() || ui->LE_imaginary_part_2number->text().isEmpty())
    {
        QMessageBox::critical(this, "Ошибка", "Не все поля заполнены!");
    }
    else
    {
        Complex_number result_number;
        Complex_number first_number;
        first_number.real_part = ui->LE_real_part_1number->text().toDouble();
        first_number.imaginary_part = ui->LE_imaginary_part_1number->text().toDouble();
        Complex_number second_number;
        second_number.real_part = ui->LE_real_part_2number->text().toDouble();
        second_number.imaginary_part = ui->LE_imaginary_part_2number->text().toDouble();
        QString result;

        switch (ui->CB_action->currentIndex())
        {
            case 1:
                result_number = sum_number(first_number, second_number);
                result = QString::number(result_number.real_part);
                ui->LE_real_result->setText(result);
                result = QString::number(result_number.imaginary_part);
                ui->LE_imaginary_result->setText(result);
            break;

            case 2:
                result_number = difference_number(first_number, second_number);
                result = QString::number(result_number.real_part);
                ui->LE_real_result->setText(result);
                result = QString::number(result_number.imaginary_part);
                ui->LE_imaginary_result->setText(result);
            break;

            case 3:
                result_number = product_number(first_number, second_number);
                result = QString::number(result_number.real_part);
                ui->LE_real_result->setText(result);
                result = QString::number(result_number.imaginary_part);
                ui->LE_imaginary_result->setText(result);
            break;

            case 4:
                if (equality_test(first_number, second_number))
                {
                    ui->LE_real_result->setText("Числа");
                    ui->LE_imaginary_result->setText("равны!");
                }
                else
                {
                    ui->LE_real_result->setText("Числа");
                    ui->LE_imaginary_result->setText("не равны!");
                }
            break;
        default:
            QMessageBox::information(this, "Внимание", "Вначале выбирите одно из\nпредложенных действий.");
        }
    }
}



void MainWindow::on_PB_information_2_clicked()
{
    QMessageBox::information(this, "Справка", "Тригонометрическая форма комплексного числа z: z = r * (cos φ + i sin φ)\nПоказательная форма комплексного числа z: z = r * e ^ (i * φ)");
}


void MainWindow::on_PB_information_clicked()
{
    QMessageBox::information(this, "Справка.", "Связь между алгебраической и тригонометрической(показательной) формой числа:\n"
                                               "Модуль r = (a + b) ^ (1/2), где a - действительная часть комплесного числа;\n"
                                               "Аргумент ф = atan(b / a)  , где b - мнимая часть комплексного числа;");
}


void MainWindow::on_comboBox_currentIndexChanged(int index)
{
    if (index == 0)
    {

        ui->PB_pow->setText("Возвести в стапень.");
    }
    else                    //index == 1
    {

        ui->PB_pow->setText("Вычислить корень.");
    }
}

void MainWindow::on_PB_transform_clicked()
{
    if(ui->LE_real_part_for_tranform->text().isEmpty() && ui->LE_imaginary_part_for_tranform->text().isEmpty() &&
            !(ui->LE_module_for_tranform->text().isEmpty() || ui->LE_argument_for_tranform->text().isEmpty()))
    {
        double real_part, imaginary_part;
        double module = ui->LE_module_for_tranform->text().toDouble();
        double argument = ui->LE_argument_for_tranform->text().toDouble();

        transform_number_from_another_form(module, argument, real_part, imaginary_part);

        ui->LE_real_part_for_tranform->setText(QString::number(real_part));
        ui->LE_imaginary_part_for_tranform->setText(QString::number(imaginary_part));
        ui->LE_module_for_tranform->clear();
        ui->LE_argument_for_tranform->clear();


    }
    else if (!(ui->LE_real_part_for_tranform->text().isEmpty() || ui->LE_imaginary_part_for_tranform->text().isEmpty()) &&
             ui->LE_module_for_tranform->text().isEmpty() && ui->LE_argument_for_tranform->text().isEmpty())
    {
        double real_part = ui->LE_real_part_for_tranform->text().toDouble();
        double imaginary_part = ui->LE_imaginary_part_for_tranform->text().toDouble();
        double module, argument;

        transform_number(module, argument, real_part, imaginary_part);

        ui->LE_module_for_tranform->setText(QString::number(module));
        ui->LE_argument_for_tranform->setText(QString::number(argument));
        ui->LE_real_part_for_tranform->clear();
        ui->LE_imaginary_part_for_tranform->clear();

    }
    else
    {
        QMessageBox::critical(this, "Ошибка", "1)Освободите поля, в которые запишется результат преобразований.\n2)Заполните все поля того числа, которое хотите преобразовать.");
    }
}
