QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_test_functions.cpp \
    main.cpp

HEADERS += \
    tst_test_functions.h

HEADERS += ../functions_pack.h

SOURCES += ../functions.cpp
