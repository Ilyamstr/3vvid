#ifndef TST_TEST_FUNCTIONS_H
#define TST_TEST_FUNCTIONS_H

#include <QtTest/QtTest>
#include <QObject>


class Test_functions : public QObject
{
    Q_OBJECT

public:
    Test_functions();

private slots:
    void test_case1();
    void test_case2();
    void test_case3();
    void test_case4();
    void test_case5();
    void test_case6();

};
#endif // TST_TEST_FUNCTIONS_H
