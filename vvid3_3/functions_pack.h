#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include <QVector>

class  Complex_number
{
    public:
    double imaginary_part;
    double real_part;
};

class Complex_number_another_form
{
    public:
    double argument;
    double module;

};

Complex_number sum_number(Complex_number first_number, Complex_number second_number);

Complex_number difference_number(Complex_number first_number, Complex_number second_number);

bool equality_test(Complex_number first_number, Complex_number second_number);

Complex_number_another_form exponentiation_number(Complex_number_another_form number, double degree);

Complex_number product_number(Complex_number first_number, Complex_number second_number);

QVector<Complex_number_another_form> radical(Complex_number_another_form number, double degree);

void transform_number_from_another_form(double module, double argument, double& real_part, double& imaginary_part);

void transform_number(double& module, double& argument, double real_part, double imaginary_part);

#endif // FUNCTIONS_H
